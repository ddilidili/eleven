from config import aws
from getopt import getopt
from botocore import session as botosession
import boto3
import sys
import os

def initialize(service):
    session = botosession.get_session()
    credentials = session.get_credentials()
    access_key = credentials.access_key
    secret_key = credentials.secret_key
    region = session.get_config_variable('region')
    client = boto3.client(
        service,
        region_name=region,
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
    )
    return client


def main():
    client = initialize('textract')
    response = client.detect_document_text(
        Document={
            'S3Object': {
                'Bucket': 'scarlett-webserver-nonprod-qa-s3bucket-b2bk1nbbw538',
                'Name': '1802-sample.jpeg'
            }
        }
    )
    print(response)


if __name__ == "__main__":
    main()
